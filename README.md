# mednickdb_pyparse
A python model for extracting and calculating relevent data and metadata from sleep files (scorefiles, edfs) and task data files (.mat, .csv). 

While these parsing functions can be used stand alone as an imported python package. Their main use is for automatically parsing uploaded files on the mednickdb. 

Interfaces with  mednickdb via the [mednickdb_pyapi](https://bitbucket.org/mednicklab/mednickdb_pysleep/src).

See the [MednickDB Manual](https://docs.google.com/document/d/18tD_ddjSYGFzIE07Uzoi0E3woXQ61TnQjqc4uXM4eXg/edit?usp=sharing) for more information

#Requirements:
[mednickdb_pysleep](https://bitbucket.org/mednicklab/mednickdb_pysleep)  
[mednickdb_pyapi](https://bitbucket.org/mednicklab/mednickdb_pysleep/src)
    
   
    
              
