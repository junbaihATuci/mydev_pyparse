import sys, os

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path + '/../mednickdb_pyparse/')
sys.path.append(my_path + '/../mednickdb_pyparse')
sys.path.append(my_path + '/../')
from parse_edf import parse_eeg_file, parse_sleep_eeg_file, write_edf_from_mne_raw_array, parse_std_eeg_file
import pyparse_defaults
import pickle
import yaml
from datetime import datetime
import mne
import warnings
import traceback


def test_edf_parse():
    study_settings = yaml.safe_load(
        open(os.path.join(os.path.dirname(__file__), 'testfiles/example1_study_settings.yaml'), 'rb'))

    sleep_eeg_filepath = os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec.edf')

    raw_eeg_data, std_edf = parse_eeg_file(sleep_eeg_filepath, study_settings)
    raw_eeg_data = raw_eeg_data[0]
    std_edf = std_edf[0]

    correct_raw_data = {'datetime': datetime(2014, 7, 8, 16, 5, 9), 'sfreq': 300.0,
                        'ch_names': ['Left Eye-A2', 'Right Eye-A1', 'C3-A2', 'C4-A1', 'Chin1-Chin2']}

    assert all([correct_raw_data[k] == v for k, v in raw_eeg_data.items()])

    assert std_edf.info['sfreq'] == pyparse_defaults.std_sfreq

    assert isinstance(std_edf, mne.io.edf.edf.RawEDF)
    write_edf_from_mne_raw_array(std_edf,
                                 os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec_std.edf'))
    assert os.path.exists(os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec_std.edf'))

    std_eeg_filepath = os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec_std.edf')
    std_eeg_data = parse_std_eeg_file(std_eeg_filepath, study_settings)[0][0]
    correct_std_data = {'datetime': datetime(2014, 7, 8, 16, 5, 9), 'sfreq': 256.0,
                        'ch_names': ['C3', 'C4', 'chin', 'LOC', 'ROC'],
                        'reference_type': 'contra_mastoid'}

    assert all([correct_std_data[k] == v for k, v in std_eeg_data.items()])


def test_parse_sleep_eeg_file():
    study_settings = yaml.safe_load(
        open(os.path.join(os.path.dirname(__file__), 'testfiles/example1_study_settings.yaml'), 'rb'))
    sleep_eeg_filepath = os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec_std.edf')
    epochstages = pickle.load(
        open(os.path.join(os.path.dirname(__file__), 'testfiles/example1_epoch_stages.pkl'), 'rb'))

    sleep_eeg_data, files_out = parse_sleep_eeg_file(sleep_eeg_filepath, epochstages, study_settings)
    sleep_features_df, band_power_per_epoch = files_out  # TODO do these look good?
    sleep_eeg_data = sleep_eeg_data[0]
    # Removed nan waso values because np.nan == np.nan is false.
    correct_sleep_eeg_data = {'datetime': datetime(2014, 7, 8, 16, 5, 9), 'sfreq': 256.0,
                               'ch_names': ['C3', 'C4', 'chin', 'LOC', 'ROC'], 'reference_type': 'contra_mastoid',
                               'n1_C3_SWA_power': 68.14203828273942, 'n1_C4_SWA_power': 74.67208350816783,
                               'n1_C3_delta_power': 43.809213974018085, 'n1_C4_delta_power': 46.069810454451805,
                               'n1_C3_theta_power': 16.468588717996276, 'n1_C4_theta_power': 16.033658852560368,
                               'n1_C3_alpha_power': 13.833068318502223, 'n1_C4_alpha_power': 14.599571133951587,
                               'n1_C3_sigma_power': 9.079615022922399, 'n1_C4_sigma_power': 22.685156083917633,
                               'n1_C3_slow_sigma_power': 11.721439635856122,
                               'n1_C4_slow_sigma_power': 13.128743399747835,
                               'n1_C3_fast_sigma_power': 7.067981674415791,
                               'n1_C4_fast_sigma_power': 26.911667314868115, 'n1_C3_beta_power': 3.1606627948591512,
                               'n1_C4_beta_power': 17.387085503596623, 'n2_C3_SWA_power': 194.68191928118978,
                               'n2_C4_SWA_power': 192.01298470326262, 'n2_C3_delta_power': 103.63061536707205,
                               'n2_C4_delta_power': 104.38567951359961, 'n2_C3_theta_power': 20.087088813985787,
                               'n2_C4_theta_power': 20.2970086137744, 'n2_C3_alpha_power': 14.04081587206252,
                               'n2_C4_alpha_power': 12.561813182786887, 'n2_C3_sigma_power': 10.499314966256375,
                               'n2_C4_sigma_power': 9.037986599743457, 'n2_C3_slow_sigma_power': 12.497458724609304,
                               'n2_C4_slow_sigma_power': 10.661702716916029,
                               'n2_C3_fast_sigma_power': 9.402435871075365, 'n2_C4_fast_sigma_power': 8.254334281891415,
                               'n2_C3_beta_power': 1.7889263494034375, 'n2_C4_beta_power': 1.5658290969265491,
                               'n3_C3_SWA_power': 251.2825342220792, 'n3_C4_SWA_power': 247.07901494700386,
                               'n3_C3_delta_power': 127.94791754525052, 'n3_C4_delta_power': 127.45227423177296,
                               'n3_C3_theta_power': 20.834173614190885, 'n3_C4_theta_power': 19.852742057534552,
                               'n3_C3_alpha_power': 13.608532517591208, 'n3_C4_alpha_power': 11.966805981905521,
                               'n3_C3_sigma_power': 7.284054061035699, 'n3_C4_sigma_power': 6.274268252323277,
                               'n3_C3_slow_sigma_power': 9.856754144082487, 'n3_C4_slow_sigma_power': 8.549477601122563,
                               'n3_C3_fast_sigma_power': 6.07182084562579, 'n3_C4_fast_sigma_power': 5.116232191237068,
                               'n3_C3_beta_power': 1.0675865214554536, 'n3_C4_beta_power': 0.9925083778530982,
                               'rem_C3_SWA_power': 50.23938312904785, 'rem_C4_SWA_power': 40.50521901626638,
                               'rem_C3_delta_power': 31.78420523992522, 'rem_C4_delta_power': 26.594018415020447,
                               'rem_C3_theta_power': 15.063995055340722, 'rem_C4_theta_power': 13.948658926792088,
                               'rem_C3_alpha_power': 11.271329600451764, 'rem_C4_alpha_power': 9.490282044301608,
                               'rem_C3_sigma_power': 5.903121507318145, 'rem_C4_sigma_power': 4.928022175643304,
                               'rem_C3_slow_sigma_power': 7.4595503382634245,
                               'rem_C4_slow_sigma_power': 6.006771041482751,
                               'rem_C3_fast_sigma_power': 4.821667664398907,
                               'rem_C4_fast_sigma_power': 4.173462230813458, 'rem_C3_beta_power': 2.2119806165305804,
                               'rem_C4_beta_power': 1.8559415345856085, 'SWA_band_hz': (0.5, 1),
                               'delta_band_hz': (1, 4), 'theta_band_hz': (4, 8), 'alpha_band_hz': (8, 12),
                               'sigma_band_hz': (11, 16), 'slow_sigma_band_hz': (11, 13),
                               'fast_sigma_band_hz': (13, 16), 'beta_band_hz': (16, 20), 'n1_spindle_av_count': 4.0,
                               'n1_spindle_av_peak_uV_in_spindle_band': 67.55101927432833,
                               'n1_spindle_av_peak_time': 0.583984375, 'n1_spindle_av_duration': 1.3203125,
                               'n1_spindle_av_freq_peak': 21.35940921823275,
                               'n1_spindle_av_density': 2.6666666666666665, 'n2_spindle_av_count': 6.0,
                               'n2_spindle_av_peak_uV_in_spindle_band': 48.93830667589136,
                               'n2_spindle_av_peak_time': 0.4466796875, 'n2_spindle_av_duration': 0.807421875,
                               'n2_spindle_av_freq_peak': 12.751049142241547,
                               'n2_spindle_av_density': 0.23529411764705882, 'n3_spindle_av_count': 1.0,
                               'n3_spindle_av_peak_uV_in_spindle_band': 50.07602760424024,
                               'n3_spindle_av_peak_time': 0.265625, 'n3_spindle_av_duration': 0.484375,
                               'n3_spindle_av_freq_peak': 12.387096774193548,
                               'n3_spindle_av_density': 0.16666666666666666,
                               'n1_slow_oscillation_av_peak_val': -41.72212481771752,
                               'n1_slow_oscillation_av_count': 4.0,
                               'n1_slow_oscillation_av_trough_time': 0.2654947916666667,
                               'n1_slow_oscillation_av_duration': 1.2352864583333334,
                               'n1_slow_oscillation_av_zero_time': 0.6088541666666667,
                               'n1_slow_oscillation_av_peak_time': 0.9919270833333333,
                               'n1_slow_oscillation_av_density': 2.666666666666667,
                               'n1_slow_oscillation_av_trough_val': 66.3844260215395,
                               'n2_slow_oscillation_av_peak_val': -51.17657974517179,
                               'n2_slow_oscillation_av_count': 178.5,
                               'n2_slow_oscillation_av_trough_time': 0.23029136058296668,
                               'n2_slow_oscillation_av_duration': 1.15895804279934,
                               'n2_slow_oscillation_av_zero_time': 0.5933275799418605,
                               'n2_slow_oscillation_av_peak_time': 0.8917745939071339,
                               'n2_slow_oscillation_av_density': 7.0,
                               'n2_slow_oscillation_av_trough_val': 82.33901401481842,
                               'n3_slow_oscillation_av_peak_val': -55.617933485914676,
                               'n3_slow_oscillation_av_count': 67.0,
                               'n3_slow_oscillation_av_trough_time': 0.2735167572463768,
                               'n3_slow_oscillation_av_duration': 1.1384519579152732,
                               'n3_slow_oscillation_av_zero_time': 0.6043116812987737,
                               'n3_slow_oscillation_av_peak_time': 0.8916627473522853,
                               'n3_slow_oscillation_av_density': 11.166666666666668,
                               'n3_slow_oscillation_av_trough_val': 74.41573631239595,
                               'rem_slow_oscillation_av_peak_val': -36.77443899534392,
                               'rem_slow_oscillation_av_count': 7.5,
                               'rem_slow_oscillation_av_trough_time': 0.32645089285714285,
                               'rem_slow_oscillation_av_duration': 1.3472377232142856,
                               'rem_slow_oscillation_av_zero_time': 0.6562848772321428,
                               'rem_slow_oscillation_av_peak_time': 0.9781668526785714,
                               'rem_slow_oscillation_av_density': 1.5,
                               'rem_slow_oscillation_av_trough_val': 54.73790786928417}

    expect = []
    for k, v in correct_sleep_eeg_data.items():
        if isinstance(v, float):
            expect.append(abs(sleep_eeg_data[k] - v) < 0.001)
        else:
            expect.append(sleep_eeg_data[k] == v)
    assert all(expect)
    # assert all([abs(sleep_eeg_data[k]-v)<0.001 for k, v in correct_sleep_eeg_data.items()])
    # assert sum([sleep_eeg_data[k] - v for k, v in correct_sleep_eeg_data.items() if isinstance(v, float)]) < 0.01

    os.remove(os.path.join(os.path.dirname(__file__), 'testfiles/example1_sleep_rec_std.edf'))
