import mne
import sys, os
my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path + '/../../mednickdb_pysleep/')
sys.path.append(my_path + '/../mednickdb_pyparse/')
from mednickdb_pysleep import scorefiles
from mednickdb_pyparse import parse_edf
import yaml
import pickle


base_file = 'C:/Users/bdyet/Desktop/subjectid22.edf'
edf = mne.io.read_raw_edf(base_file)
study_settings = yaml.safe_load(open('C:/Users/bdyet/GoogleDrive/mednickdb_pyparse/tests/testfiles/example1_study_settings.yaml','rb'))
epochstages, starttime = scorefiles.extract_epochstages_from_scorefile(base_file, study_settings['stage_map'])
crop_start = 200*60
crop_end = 250*60
epoch_len = 30
epoch_start = int(crop_start/epoch_len)
epoch_end = int(crop_end/epoch_len)
n_epochs = epoch_end-epoch_start
epochstages = epochstages[epoch_start:epoch_end]
assert len(epochstages)*epoch_len == crop_end-crop_start

edf.load_data()
print(edf.ch_names)
edf = edf.crop(tmin=crop_start, tmax=crop_end)
scoring = edf.copy().pick_channels(['EDF-Annotations'])
parse_edf.write_edf_from_mne_raw_array(scoring, 'C:/Users/bdyet/Desktop/example1_sleep_scoring.edf', annotations=True)

edf = edf.copy().pick_channels(['Left Eye-A2', 'Right Eye-A1', 'C3-A2', 'C4-A1', 'Chin1-Chin2'])
edf = edf.resample(300)
parse_edf.write_edf_from_mne_raw_array(edf, 'C:/Users/bdyet/Desktop/example1_sleep_rec.edf')


pickle.dump(epochstages, open('C:/Users/bdyet/Desktop/example1_epoch_stages.pkl', 'wb'))

