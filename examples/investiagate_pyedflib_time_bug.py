import mne
import sys, os
my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path + '/../../mednickdb_pysleep/')
sys.path.append(my_path + '/../mednickdb_pyparse/')
from mednickdb_pyparse import parse_edf
from datetime import datetime, timedelta
from wonambi import Dataset
import time

filename = 'subjectid22'
edf = mne.io.read_raw_edf('C:/Users/bdyet/Desktop/'+filename+'_base.edf')
won_edf = Dataset('C:/Users/bdyet/Desktop/'+filename+'_base.edf')
meas_data_base = datetime.fromtimestamp(edf.info['meas_date'][0]) + timedelta(seconds=time.altzone)
edf.load_data()
parse_edf.write_edf_from_mne_raw_array(edf, 'C:/Users/bdyet/Desktop/'+filename+'_saved.edf')

edf_saved = mne.io.read_raw_edf('C:/Users/bdyet/Desktop/'+filename+'_saved.edf')
meas_data_saved = datetime.fromtimestamp(edf_saved.info['meas_date'][0]) + timedelta(seconds=time.altzone)
assert meas_data_base == meas_data_saved
print(meas_data_saved - meas_data_base)