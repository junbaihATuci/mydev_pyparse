#!/bin/bash

echo "start setup microservice environment"
mv src/mednickdb-pyparse/mednickdb_pyparse mednickdb_pyparse
echo "finished pyparse"
mv src/mednickdb-pysleep/mednickdb_pysleep mednickdb_pysleep
echo "finished pysleep"
mv src/mednickdb-pyapi/mednickdb_pyapi mednickdb_pyapi
echo "finished pyapi"
rm -rf src
echo "done"
