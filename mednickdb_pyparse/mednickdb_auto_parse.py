import os
import sys
from inspect import signature
import time
import warnings
import logging
import queue
import threading
from mednickdb_pyapi.pyapi import MednickAPI, ServerError
from typing import Union, List, Dict, Tuple #TODO add type hints to all functions
from mednickdb_pyparse import parse_dispatcher
from mednickdb_pyparse.error_handling import ParseWarning, ParseError
import shutil
import sys
from enum import Enum

debug = True

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    handlers=[
        logging.FileHandler("errors.log"),
        logging.StreamHandler()
    ])


class DebugLevel(Enum):
    """

    """
    PROD = 0
    WARNING = 1
    ALL = 2


class AutomatedParsingManager:
    """
    The automated data parsing manager class for auto parsing service.It maintains only one connection with the server
    all the time to protect the performance of the server.
    """

    def __init__(self, max_attempt=10, parse_rate=3, qsize=0, problem_files=[],
                 compute_heavy_files=('raw_eeg', 'raw_sleep_eeg', 'std_sleep_eeg', 'std_eeg'), debug=DebugLevel.ALL ):
        """
        Constructor of the class.Notice that all parameters could only be configured in the constructor

        :param: max_attempt: integer for the maximum number of consecutive unsuccessful reconnection allowed before
            throwing ConnectionError. The default value is 10
        :param: parse_rate: the interval (second) between each request for unparsed_files. The default value is 3
        :param: qsize: the size of queue for unparsed_files. The default value is 0, meaning no size restriction.
        :param: problems_files: list of problem files that will be skipped directly. The default value is []
        :param: compute_heavy_files: list of fileformats that requires a separated thread to parse.
            The default value is ['raw_eeg', 'raw_sleep_eeg', 'std_sleep_eeg', 'std_eeg']
        """
        self.lock = threading.Lock()
        self.file_in_process = set()
        self.login_max_attempt = max_attempt
        self.login_attempt = 0
        self.problem_files = set(problem_files)
        self.waiting_files = set() #files that are waiting for dependences. We check these less often.
        self.last_wating_parse = time.time()
        self.computer_heavy_files = set(compute_heavy_files)
        self.parse_rate = parse_rate
        self.input_queue = queue.Queue(qsize)
        self.output_queue = queue.Queue()  #
        self.client = None
        self.debug = debug
        if os.path.exists('tempfiles/'):
            shutil.rmtree('tempfiles/')  # flush old temp files
        os.mkdir('tempfiles/')  # make dir for temp files to be uploaded from
        self._client_login()

    def _client_login(self, reconnect_interval=5):
        """
        login function to create a new connection with the server, either to establish
        the first connection, or to reconnect if any network ConnectionError is detected.
        If self.login_max_attempt is reached, ConnectionError will be thrown. When a connection
        is successful establish, new value will be assigned to self.client

        :param reconnect_interval: interval (second) between each connection attempt. The default value is 5
        """
        password = os.environ['MEDNICKDB_DEFAULT_PW']
        server_address = os.environ['MEDNICKDB_SERVER_ADDRESS'] + ':' + os.environ['MEDNICKDB_API_PORT_NUMBER']

        while True:
            try:
                self.client = MednickAPI(server_address=server_address,
                                         username='mednickdb.microservices@gmail.com',
                                         password=password)
            except ConnectionError as e:
                print(e)
                if self.login_attempt < self.login_max_attempt:
                    self.login_attempt += 1
                    print("reconnect in %d sec" % reconnect_interval)
                    time.sleep(reconnect_interval)
                    continue
                else:
                    print("reconnection failed")
                    raise e

            self.login_attempt = 0
            return

    def run_service(self):
        """
        the enter point of the whole service. It constantly requests unparsed files, prepares files for the parsing algorithm
        (see the documentation of self._preparse_files), and starts parsing subroutines for files that are ready to parse (see
        the documentation of self._process_files).
        """
        print('Auto-parse Running')
        while True:
            try:
                file_infos = self.client.get_unparsed_files(previous_versions=False)  # blocking
            except ConnectionError:
                time.sleep(5)
                continue
            except ServerError:  # TODO explicitly check whether the token is expired or not
                # re connect to the server if the token already expired
                self._client_login()
                continue

            file_infos = self._remove_waiting_files(file_infos)

            if len(file_infos) > 0:  # There are still files to parse
                if self.debug:
                    print('Found', len(file_infos), 'unparsed files\n',file_infos,'\nbeginning parse:')
                else:
                    print('Found', len(file_infos), 'unparsed files', 'beginning parse:')

                self._prepare_files(file_infos)
                self._process_files()
            else:
                print('Completed parse. Sleeping for', self.parse_rate,
                  'seconds. Logger has',str(len(self.problem_files)),'problem files',
                  'and',str(len(self.waiting_files)),'files waiting for dependencies.')
                time.sleep(self.parse_rate)


    def _remove_waiting_files(self, files):
        """
        remove all the files waiting for dependences for the list of give files
        :param files: the files to check waiting status for
        :return: files that are not waiting for dependences
        """
        if time.time() - self.last_wating_parse > 10:
            print('10 seconds elapsed, trying waiting files again')
            self.last_wating_parse = time.time()
            with self.lock:
                self.waiting_files = set()  # empty waiting files, they will all parse now
                return files #parse em all!

        non_waiting_files = []
        for file in files:
            with self.lock:
                if file['_id'] not in self.waiting_files:
                    non_waiting_files.append(file)

        return non_waiting_files


    def _prepare_files(self, files):
        """
        Skips files that are already in processing, and adds new ones to self.input_queue to parse.
        This method is thread safe.

        :param files: list of file information dict
        """
        for file in files:
            with self.lock:
                if file['_id'] not in self.file_in_process:
                    print('Found', file, 'in unparsed files, beginning parse.')
                    self.file_in_process.add(file['_id'])
                    self.input_queue.put(file)
                    print('{} added to processing list'.format(file['_id']))

    def _process_files(self):  # batch processing about to work, but the processing flow is awkward
        """
        Takes a fileinfo from self.input_queue, checks if its file name is in self.problem files. If so, skip it, otherwise,
        checks if the fileformat would take a long time to parse and therefore requires a separated thread. Starts the parsing
        subroutine for the file.(see the description of self._parsing_wrapper). After one file is parsed, this method sends
        output data in self.output_queue to the server if any exists. If the change_status value of the output data is true,
        a request to change 'parsed' field value of a specific record will be sent to the server, and the fid of that file will
        be removed from self.file_in_process, indicating the parsing for the file is completed.
        :return:
        """
        while True: #TODO the logic of this is not ideal, we should push out to all workers and wait till they return
            # process input files
            # print('main thread' + str(threading.get_ident()))
            try:
                file_info = self.input_queue.get(False)
            except queue.Empty:
                print("input queue empty, proceeds to output queue")
                pass
            else:
                if file_info['filename'] in self.problem_files:
                    continue
                if file_info['fileformat'] in self.computer_heavy_files:
                    print('heavy computing work found')
                    worker = threading.Thread(target=self._parsing_wrapper, args=(file_info,), daemon=True)
                    print('new thread dispatched')
                    worker.start()
                else:
                    self._parsing_wrapper(file_info)
            # process generated files
            while True:
                try:
                    parsing_result = self.output_queue.get(False)
                except queue.Empty:
                    print("output queue empty, return from current processing iteration")
                    return
                else:
                    try:
                        data, file, fid, data_or_file_specifiers, error, change_status = parsing_result[0:6]
                        if error:
                            raise error
                        print(parsing_result)
                        if data is not None and data_or_file_specifiers:
                            print('Main thread uploading data')
                            self.client.upload_data(data=data, fid=fid, **data_or_file_specifiers)
                        if file is not None and data_or_file_specifiers:
                            print('Main thread uploading a file')
                            file_info = self.client.upload_file(fileobject=file, **data_or_file_specifiers)
                            print('Main thread uploaded a file as fid:', file_info['_id'])
                        if change_status and change_status is not None:
                            with self.lock:
                                print('change parse status of ' + fid + ' to parsed')
                                self.file_in_process.remove(fid)
                                self.client.update_parsed_status(fid=fid, status=True)
                        if change_status is None:
                            with self.lock:
                                print('File',fid,'waiting for dependencies, adding to waiting queue')
                                self.file_in_process.remove(fid)
                                self.waiting_files.add(fid)
                    except ConnectionError:
                        self._client_login()
                        self.output_queue.put(parsing_result)  # when disconnected, put result back to the queue

    def _parsing_wrapper(self, base_file_info):
        """
        Runs the appropriate parsing algorithm based on the file_info (see the description of parse_file_on_fileformat)
        Adds all output data to self.output_queue to send to the server by self.process_files.Each output data is a tuple
        of (output_data, output_file, fid, file_specifier, error, change_status_flag). The change_status_flag is False, unless the output data is
        the last one from its source file. If no output data is generated, output_data and file_specifier fields are None,
        and change_status_flag is set to True, indicating the file is parsed but no data to send back.
        If some error occurs, this is logged but not raised too, so that the regular db can continue as normal.
        TODO: we should probably alert an admin in this case (automatic email, slack webhook)

        :param base_file_info: a dict of file information to parse
        """
        default_specifiers = ['subjectid','versionid','studyid','visitid','sessionid','filetype']

        try:
            print('\r thread ' + str(threading.get_ident()) + ' Working on ' + base_file_info['filename'] + ' with fid ' +
                  base_file_info['_id'])
            start_time = time.time()
            data_upload_kwargs = set([k for k, v in signature(self.client.upload_data).parameters.items()]) | set(default_specifiers)
            file_upload_kwargs = set([k for k, v in signature(self.client.upload_file).parameters.items()]) | set(default_specifiers)
            base_file_specifiers = {k: v for k, v in base_file_info.items() if k in data_upload_kwargs}
            trigger_warning = False
            with warnings.catch_warnings():
                warnings.filterwarnings("error", category=ParseWarning)
                try:
                    datas_out, files_out = parse_dispatcher.dispatch_to_parser(
                        med_api=self.client,
                        file_specifiers=base_file_specifiers,
                        fileformat=base_file_info['fileformat'],
                        filepath=base_file_info['filepath'],
                        get_files_from_server_storage=True)
                except ParseWarning as w: #this is stupid. Im trying to catch warnings, and then do something and re-raise
                    self.output_queue.put((None, None, base_file_info['_id'], None, None, None)) #tell parent not to fix parse flag
                    trigger_warning = w
                    print('Could not parse file, not setting parsed flag')
                    print(str(w))
                except ParseError as e:
                    trigger_warning = e
                    print('Problems parsing file. Error was:\n')
                    print(str(e))

            if trigger_warning:
                warnings.warn(trigger_warning)
                return


            last_flag = False
            if datas_out is not None or files_out is not None:
                if datas_out is not None: # data to upload
                    for idx, data in enumerate(datas_out):
                        #del data['filepath']  # not a file, there is no path #TODO these should not be here
                        #del data['fileformat']  # we don't want to have dead refs, and fid will fill this role

                        # Any fields that are important to data upload need to be copied from existing file:
                        data_keys = list(data.keys()) # because data changes in loop below
                        data_specifiers = {k: v for k, v in base_file_info.items() if k in data_upload_kwargs}
                        data_specifiers.update({k: data.pop(k) for k in data_keys if k in data_upload_kwargs})
                        assert 'subjectid' in data_specifiers, "subjectid is required for data upload, and was not provided as file info, or was not present in file"
                        # push to queue and upload in main thread. #FIXME will large uploads halt main thread??
                        self.output_queue.put((data, None, base_file_info['_id'], data_specifiers, False, False))
                        if debug:
                            print('\r ' + 'thread ' + str(threading.get_ident()) + ' completed data row', idx + 1, 'of',
                                  len(datas_out), 'for',
                                  base_file_info['filename'], "enqueued to upload")

                if files_out is not None: # files to upload
                    for idx, new_file_info in enumerate(files_out):
                        file_pointer = new_file_info.pop('file')

                        # Any fields that are important to data upload need to be copied from existing file:
                        base_fid = base_file_info['_id']  # remove master files id
                        file_keys = list(new_file_info.keys())  # because data changes in loop below
                        new_file_specifiers = {k: v for k, v in base_file_info.items() if k in file_upload_kwargs}
                        new_file_specifiers.update({k: new_file_info.pop(k) for k in file_keys if k in file_upload_kwargs})
                        if idx == len(files_out) - 1:
                            last_flag = True
                        self.output_queue.put((None, file_pointer, base_fid, new_file_specifiers, False, last_flag))
                        if debug:
                            print('\r ' + 'thread ' + str(threading.get_ident()) + ' completed file creation', idx + 1, 'of',
                                  len(files_out), 'for',
                                  base_file_info['filename'], "enqueued to upload")
                else:
                    self.output_queue.put((None, None, base_file_info['_id'], None, False, True))  # trigger last_flag, to shut down worker

            else:
                self.output_queue.put((None, None, base_file_info['_id'], None, False, True)) #nothing to upload

            # med_api.update_parsed_status(fid=file_info['_id'], status=True)
            end_time = time.time()
            print('\r ' + str(end_time - start_time) + ' seconds elapsed')
        except Exception as e:  # some kind of parsing error on a specific file
            if debug:
                #You cannot raise a exception and catch it in the parent thread... so push error to parent
                self.output_queue.put((None,None,base_file_info['_id'],None,e,True))

            else:
                print(e)
                #FIXME should raise ParseError which should handle all this problem file stuff.
                self.problem_files.add(base_file_info['filename'])
                logging.exception('Problem file: ' + base_file_info['filename']) #FIXME Logging needs overhall.


if __name__ == '__main__':
    """Here is the entry to the saclab servers parsing script"""
    parse_rate = 3  # seconds per DB query
    problem_files = []
    manager = AutomatedParsingManager(parse_rate=parse_rate, problem_files=problem_files)
    manager.run_service()
