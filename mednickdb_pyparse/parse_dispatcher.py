import os
import warnings
from typing import Union, List, Dict, Tuple
from mednickdb_pyparse.parse_scorefile import parse_scorefile
from mednickdb_pyparse.parse_edf import parse_eeg_file, parse_sleep_eeg_file, parse_std_eeg_file, write_edf_from_mne_raw_array
from mednickdb_pyparse.parse_tabular import parse_tabular_file
from mednickdb_pyparse.error_handling import ParseError, ParseWarning
from mednickdb_pyapi.pyapi import MednickAPI
import pickle
import yaml
from datetime import datetime

uploads_prefix = '' if os.environ['HOME'] is None else '/data/mednick_server/' #if not running in docker


def dispatch_to_parser(file_specifiers=None, get_files_from_server_storage=False, med_api=None, **kwargs: Union[dict, str, int]) -> Tuple[Union[List[dict], None], Union[List[dict], None]]:
    """
    Parses the file at fileinfo['filepath']. Data and meta data are extracted according to the rules specified by
    file_info['fileformat'].
    Returns a list of data dicts to be uploaded, and a list of files to be uploaded (or None ir no files).


    This function is called by default on any file upload and will run if fileformat matches any of
    ['raw_eeg', 'raw_sleep_eeg', 'std_eeg', 'sleep_scoring', 'std_sleep_eeg', 'tabular'].
    The following kwargs are required for each fileformat:
    tabular: None
    raw_eeg: studyid and versionid
    raw_sleep_eeg: studyid and versionid
    std_eeg: studyid and versionid
    std_sleep_eeg: studyid, versionid, and any other specifiers that match the level of that data so that the correct sleep_scoring can be grabbed
    sleep_scoring: studyid and versionid

    :param: file_specifiers: file_specifiers dict, with at least filepath, fileformat, studyid of the file to parse (as downloaded from database).
    :param: get_files_from_server_storage: Weather to try to load the given files from the server, or just use the given path
    :param kwargs: some of all of the individual keys and values of the file info object (i.e. **file_info).
    minimum set is filepath, fileformat, studyid. Useful when parsing without the need for a server (i.e. just extracting data from files)
    :param: get_files_from_server_storage: If files paths should be reletive to server file storage location, or where this file is run from.
    :return: data object to upload to the database, may addtionally return files to post to database also
    """
    if file_specifiers is None:
        file_specifiers = kwargs

    elif kwargs is not None:
        file_specifiers.update(kwargs)

    file_path = file_specifiers['filepath']
    if get_files_from_server_storage:
        file_path = uploads_prefix + file_path

    # get study settings
    if file_specifiers['fileformat'] in ['raw_eeg', 'raw_sleep_eeg', 'std_eeg', 'sleep_scoring', 'std_sleep_eeg']:
        study_settings = get_study_settings(file_specifiers, med_api)
        if study_settings is None:
            return None, None

    kwargs_req_map = {
        'study_settings': [],
        'sleep_features':[],
        'band_power_per_epoch': [],
        'raw_eeg': ['studyid', 'versionid'],
        'raw_sleep_eeg': ['studyid', 'versionid'],
        'std_eeg': ['studyid', 'versionid'],
        'std_sleep_eeg': ['studyid', 'versionid'],
        'sleep_scoring': ['studyid', 'versionid'],
        'tabular': []
    }
    if file_specifiers['fileformat'] in kwargs_req_map:
        req_kwargs = kwargs_req_map[file_specifiers['fileformat']]
        for kwarg in req_kwargs:
            if kwarg not in file_specifiers:
                raise ValueError('Fileformat', file_specifiers['fileformat'], 'requires', kwarg)
    else:
        warnings.warn('\nfileformat of {} {} is unknown, skipping'.format(file_specifiers['fileformat'],
                                                                          file_specifiers['filepath']), ParseWarning)
        return None, None

    # map from fileformat to dispatch/parse function
    dispatch_func_map = {
        'study_settings': lambda f: (None, None),
        'sleep_features': lambda f: (None, None),
        'band_power_per_epoch': lambda f: (None, None),
        'raw_eeg': lambda f: dispatch_eeg_file(f, file_specifiers, study_settings),
        'raw_sleep_eeg': lambda f: dispatch_eeg_file(f, file_specifiers, study_settings),
        'std_eeg': lambda f: parse_std_eeg_file(f, study_settings),
        'std_sleep_eeg': lambda f: dispatch_sleep_eeg_file(f, file_specifiers, study_settings, med_api=med_api),
        'sleep_scoring': lambda f: parse_scorefile(f, study_settings),
        'tabular': parse_tabular_file
                         }

    if file_specifiers['fileformat'] in dispatch_func_map:
        datas_ret, files_ret = dispatch_func_map[file_specifiers['fileformat']](file_path)
    else:
        warnings.warn('\nfileformat of {} {} is unknown, skipping'.format(file_specifiers['fileformat'],
                                                                          file_specifiers['filepath']), ParseWarning)
        return None, None

    return datas_ret, files_ret


def dispatch_eeg_file(file_path, file_specifiers, study_settings):
    std_data, std_file = parse_eeg_file(file_path, study_settings)
    if std_file is not None:
        std_file = std_file[0]
        base_file_name_parts = os.path.basename(file_specifiers['filepath']).split('__')
        base_file_name = '__'.join(base_file_name_parts[:-1])
        std_file_temppath = 'tempfiles/'+base_file_name + '_std.edf'
        write_edf_from_mne_raw_array(std_file, std_file_temppath)
        std_file_info = [{'fileformat': file_specifiers['fileformat'].replace('raw', 'std'),
                         'filetype': file_specifiers['fileformat'].replace('raw', 'std'),
                         'file': open(std_file_temppath, 'rb')}]
    else:
        std_file_info = None
    return std_data, std_file_info


def dispatch_sleep_eeg_file(file_path, file_specifiers, study_settings, med_api):
    assert med_api, "cannot parse sleep_eeg without access to the scorefile via the med_api"

    scorefile_specifiers = file_specifiers.copy()
    scorefile_specifiers['filetype'] = 'sleep_scoring'
    base_file_name_parts = os.path.basename(file_specifiers['filepath']).split('__')
    base_file_name = '__'.join(base_file_name_parts[:-1])
    scorefile_specifiers.pop('fileformat')
    scorefile_specifiers.pop('filepath')
    scorefiles = med_api.get_data(**scorefile_specifiers, format='flat_dict')
    if len(scorefiles) <= 0:
        warnings.warn('Could not parse sleep_eeg file'+file_specifiers['filepath']+\
                      ' because a corresponding scorefile was not found', ParseWarning)
        return None, None
    epochstages = scorefiles[0]['sleep_scoring.epochstages']
    lights_off_seconds = scorefiles[0]['sleep_scoring.lights_off']
    lights_on_seconds = scorefiles[0]['sleep_scoring.lights_on']
    data, files = parse_sleep_eeg_file(file_path, epochstages, study_settings,
                                       start_offset=lights_off_seconds,
                                       end_offset=lights_on_seconds)
    sleep_feature_data = data[0]
    sleep_features_df, band_power_per_epoch = files

    # package into stuff the pyapi can deal with
    new_filename = 'tempfiles/' + base_file_name + '_sleep_features.csv'
    sleep_features_df.to_csv(new_filename)
    sleep_features_file_info = {'fileformat': 'sleep_features',
                                'filetype': 'sleep_features',
                                'file': open(new_filename, 'rb')}

    new_filename = 'tempfiles/' + base_file_name + '_band_power_per_epoch.pkl'
    pickle.dump(band_power_per_epoch, open(new_filename,'wb'))
    band_power_file_info = {'fileformat': 'band_power_per_epoch',
                            'filetype': 'band_power_per_epoch',
                            'file': open(new_filename, 'rb')}

    return [sleep_feature_data], [sleep_features_file_info, band_power_file_info]


def get_study_settings(file_specifiers: dict, med_api: MednickAPI) -> Union[None, dict]:
    """
    Return settings for a study by loading study_settings.yaml file [filetype=study_settings].
    Queries the file store via mednick_pyapi for this file.
    :param file_specifiers: specifiers for the current file being parsed
    :param med_api: ref to the MednickAPI
    :return: study settings
    :raises: ParseError if study settings cannot be found
    """
    study_settings_infos = med_api.get_files(studyid=file_specifiers['studyid'],
                                             versionid=file_specifiers['versionid'],
                                             filetype='study_settings')
    if len(study_settings_infos) <= 0:
        print('Study settings missing.')
        raise ParseError('Could not parse '+file_specifiers['filepath']+
                      " because a corresponding study_settings file was not found in server's filestore")

    study_settings_info = study_settings_infos[0]
    if 'uploads' in study_settings_info['filepath']: #FIXME, hacky so we don't append for test. There must be a better way
        study_settings_info['filepath'] = uploads_prefix + study_settings_info['filepath']

    with open(study_settings_info['filepath'], 'rb') as f:
        return yaml.safe_load(f)



