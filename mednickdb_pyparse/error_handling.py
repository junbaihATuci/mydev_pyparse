
class ParseError(BaseException):
    """Custom error when parsing cannot complete
    This should never happen. Something has gone wrong.
    """
    pass

class ParseWarning(Warning):
    """Custom warning when parsing cannot complete
    This might happen occasionally, and is not necessarily something bad happening
    """
    pass