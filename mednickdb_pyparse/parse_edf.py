import mne
from datetime import datetime
import os
from mednickdb_pysleep import sleep_features, frequency_features, pysleep_defaults, pysleep_utils
from mednickdb_pyparse import pyparse_defaults
import pandas as pd
from typing import List, Tuple, Union
import re
import warnings
import pyedflib
import wonambi.ioeeg.edf as wnbi


def parse_eeg_file(path: str, study_settings: dict):
    """
    extract metadata from file @path and return in a dictionary. Only supports EDF at present.
    TODO extend for all eeg type files that mne can process
    :param path: path of edf to parse
    :return:
    """

    if os.path.splitext(path)[-1].lower() != '.edf':
        NotImplementedError("Only EDFs are supported currently. More files coming.")


    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        try:  # edf
            edf = mne.io.read_raw_edf(path, stim_channel=None, verbose=False)
        except RuntimeError:  # edf+
            edf = mne.io.read_raw_edf(path, preload=True, stim_channel=None, verbose=False)

    # TODO edf++, .eeg, .?
    wnbi_edf = wnbi.Edf(path) #MNE screws up the dates, so lets not use it here

    raw_eeg_data = {
        'datetime': wnbi_edf.hdr['start_time'],
        'sfreq': edf.info["sfreq"],
        'ch_names': edf.ch_names,
    }

    if study_settings is not None:
        std_edf = standardize_edf(edf, study_settings)
        std_edf.info['meas_date'] = (raw_eeg_data['datetime'].timestamp(), 0)
        std_edfs = [std_edf]
    else:
        std_edfs = None
        warnings.warn('std_eeg not created because study settings is missing')

    return [raw_eeg_data], std_edfs


def parse_std_eeg_file(path: str, study_settings: dict):

    if os.path.splitext(path)[-1].lower() != '.edf':
        NotImplementedError("Only EDFs are supported currently. More files coming.")

    try:  # edf
        std_edf = mne.io.read_raw_edf(path, stim_channel=None, verbose=False)
    except RuntimeError:  # edf+
        std_edf = mne.io.read_raw_edf(path, preload=True, stim_channel=None, verbose=False)

    wnbi_edf = wnbi.Edf(path)  # MNE screws up the dates, so lets not use it here

    std_eeg_data = {
        'datetime': wnbi_edf.hdr['start_time'],
        'sfreq': std_edf.info["sfreq"],
        'ch_names': std_edf.ch_names,
        'reference_type': study_settings['ref_type']
    }

    return [std_eeg_data], None


def parse_sleep_eeg_file(filepath, epochstages, study_settings, start_offset=None, end_offset=None):
    """
    From a standardized edf file, extract relevent sleep features, incuding:
    spindles, so, rem, band power - per channel, per stage (per quartile - TODO)
    """

    sleep_feature_data = parse_std_eeg_file(filepath, study_settings)[0][0]
    band_power_per_epoch, bands, chans = frequency_features.extract_band_power_per_epoch(edf_filepath=filepath,
                                                                                         start_time=start_offset,
                                                                                         end_time=end_offset,
                                                                                         chans_to_consider=study_settings['chans_for_band_power'])

    band_power_per_stage = frequency_features.extract_band_power_per_stage(band_power_per_epoch,
                                                                           epochstages,
                                                                           stages_to_consider=pysleep_defaults.stages_to_consider)

    for stage, band_power_in_stage in band_power_per_stage.items():
        for band_idx, band in enumerate(bands):
            for chan_idx, chan in enumerate(chans):
                sleep_feature_data[stage + '_' + chan + '_' + band.replace('band_hz', 'power')] \
                    = band_power_in_stage[chan_idx, band_idx]

    sleep_feature_data.update(bands)

    #%% Sleep Features
    features_detected = []
    if 'chans_for_spindles' in study_settings and study_settings['chans_for_spindles']:
        spindles = sleep_features.detect_spindles(edf_filepath=filepath, chans_to_consider=study_settings['chans_for_spindles'])
        features_detected.append(spindles)

    if 'chans_for_slow_oscillations' in study_settings and study_settings['chans_for_slow_oscillations']:
        sos = sleep_features.detect_slow_oscillation(edf_filepath=filepath, chans_to_consider=study_settings['chans_for_slow_oscillations'])
        features_detected.append(sos)

    if 'chans_for_rems' in study_settings and study_settings['chans_for_rems'] and pysleep_defaults.load_matlab_detectors:
        rems = sleep_features.detect_rems(edf_filepath=filepath, epochstages=epochstages, loc_chan='LOC', roc_chan='ROC')
        features_detected.append(rems)

    feature_events_cont = []
    for feature in features_detected:
        feature_events = sleep_features.assign_stage_to_feature_events(feature, epochstages)
        feature_events_cont.append(feature_events)
        features_per_stage = sleep_features.sleep_feature_variables_per_stage(feature_events, epochstages,
                                                                stages_to_consider=pysleep_defaults.stages_to_consider)
        feature_dict = {}
        feature_vars = set(features_per_stage.columns) - {'stage', 'description', 'eventtype'}
        for idx, row in features_per_stage.iterrows():
            for feature_var in feature_vars:
                feature_dict[row['stage'] + '_' + row['description'] + '_' + feature_var] = row[feature_var]
        sleep_feature_data.update(feature_dict)
    sleep_features_df = pd.concat(feature_events_cont, axis=0)

    return [sleep_feature_data], [sleep_features_df, band_power_per_epoch]


def standardize_edf(edf: mne.io.RawArray, study_settings: dict) -> mne.io.RawArray:

    chan_map = study_settings['known_eeg_chans']
    if 'known_emg_chans' in study_settings and study_settings['known_emg_chans']:
        chan_map.update(study_settings['known_emg_chans'])
    if 'known_eog_chans' in study_settings and study_settings['known_eog_chans']:
        chan_map.update(study_settings['known_eog_chans'])
    if 'known_ecg_chans' in study_settings and study_settings['known_ecg_chans']:
        chan_map.update(study_settings['known_ecg_chans'])

    ## Names:
    edf.rename_channels(chan_map)

    # set chan types #TODO

    # Load required by resample
    edf.load_data()

    # Drop annotatations (they are stored in the db)
    annot_chans = [ch for ch in edf.ch_names if 'EDF Annotations' in ch]
    edf = edf.drop_channels(annot_chans)

    # resample
    edf = edf.resample(pyparse_defaults.std_sfreq)

    # filter:
    eeg_chan_idx = [i for i, ch in enumerate(edf.ch_names) if ch in list(study_settings['known_eeg_chans'].values())]
    edf = edf.filter(pyparse_defaults.std_high_pass_hz,
                     pyparse_defaults.std_low_pass_hz,
                     picks=eeg_chan_idx)

    # Reorder channels:
    chan_names_to_order = edf.ch_names
    known_emg_list = list(study_settings['known_emg_chans'].values()) if study_settings['known_emg_chans'] else None
    known_eog_list = list(study_settings['known_eog_chans'].values()) if study_settings['known_eog_chans'] else None
    known_ecg_list = list(study_settings['known_ecg_chans'].values()) if study_settings['known_ecg_chans'] else None
    ordering = list(map(lambda x: get_ordering(x,
                                               known_emg_list,
                                               known_eog_list,
                                               known_ecg_list),
                        edf.ch_names))

    ordering_ordered = [i for i, x in sorted(enumerate(ordering), key=lambda x: x[1:])]
    edf = edf.reorder_channels([chan_names_to_order[o] for o in ordering_ordered])

    return edf


def get_ordering(chan: str, known_emg_channels, known_eog_channels, known_ecg_channels) -> Tuple[Union[int,None], Union[int,None]]:
    """
    Define where a channel is located on the x, y plain, can be used for ordering
    Order will be (None, None) if channel is unknown or:
        for EMG: -1*(number of EMG + number of EOG channels)
        for EOG: -1*(number of EOG channels)
    :param: chan: The eeg channel name
    :return: order_dataframe: A df with columns [X sort order, Y sort order, chan]

    """

    order_x = [str(i) for i in range(9, 0, -2)] + ['z'] + [str(i) for i in range(0, 11, 2)]
    order_y = {'Fp': 1, 'AF': 2, 'F': 3, 'FC': 3, 'FT': 4, 'T': 5, 'C': 5, 'TP': 6, 'CP': 6, 'P': 7, 'PO': 8, 'O': 9}

    if known_emg_channels and chan in known_emg_channels:
        idx = known_emg_channels.index(chan) + 1000  # assuming we will never have more than 1000 eeg channels (safe af)
        return idx, idx

    if known_eog_channels and chan in known_eog_channels:
        idx = known_eog_channels.index(chan) + 2000
        return idx, idx

    if known_ecg_channels and chan in known_ecg_channels:
        idx = known_ecg_channels.index(chan) + 3000
        return idx, idx

    try:
        x = re.findall(r'[0-9z]+', chan)[0]
        y = re.findall(r'[a-yA-Y]+', chan)[0]
        return order_x.index(x), order_y[y]
    except (IndexError, ValueError, KeyError):
        return 5000, 5000


def write_edf_from_mne_raw_array(mne_raw: mne.io.RawArray, fname: str, annotations=False, new_date=False, picks=None, tmin=0, tmax=None, overwrite=True):
    """
    Saves the raw content of an MNE.io.Raw and its subclasses to
    a file using the EDF+ filetype
    pyEDFlib is used to save the raw contents of the RawArray to disk
    Parameters
    ----------
    mne_raw : mne.io.Raw
        An object with super class mne.io.Raw that contains the data
        to save
    fname : string
        File name of the new dataset. This has to be a new filename
        unless data have been preloaded. Filenames should end with .edf
    picks : array-like of int | None
        Indices of channels to include. If None all channels are kept.
    tmin : float | None
        Time in seconds of first sample to save. If None first sample
        is used.
    tmax : float | None
        Time in seconds of last sample to save. If None last sample
        is used.
    overwrite : bool
        If True, the destination file (if it exists) will be overwritten.
        If False (default), an error will be raised if the file exists.
    """
    if not issubclass(type(mne_raw), mne.io.BaseRaw):
        raise TypeError('Must be mne.io.Raw type')
    if not overwrite and os.path.exists(fname):
        raise OSError('File already exists. No overwrite.')
    # static settings
    if annotations:
        file_type = pyedflib.FILETYPE_EDFPLUS
    else:
        file_type = pyedflib.FILETYPE_EDF

    sfreq = mne_raw.info['sfreq']
    date = datetime.now().strftime('%d %b %Y %H:%M:%S') if new_date \
        else (datetime.fromtimestamp(mne_raw.info['meas_date'][0])).strftime('%d %b %Y %H:%M:%S')
    first_sample = int(sfreq*tmin)
    last_sample  = int(sfreq*tmax) if tmax is not None else None


    # convert data
    channels = mne_raw.get_data(picks,
                                start = first_sample,
                                stop  = last_sample)

    # convert to microvolts to scale up precision
    channels *= 1e6

    # set conversion parameters
    dmin, dmax = [-32768,  32767]
    pmin, pmax = [channels.min(), channels.max()]
    n_channels = len(channels)

    # create channel from this
    try:
        f = pyedflib.EdfWriter(fname,
                               n_channels=n_channels,
                               file_type=file_type)

        channel_info = []
        data_list = []

        for i in range(n_channels):
            ch_dict = {'label': mne_raw.ch_names[i],
                       'dimension': 'uV',
                       'sample_rate': sfreq,
                       'physical_min': pmin,
                       'physical_max': pmax,
                       'digital_min':  dmin,
                       'digital_max':  dmax,
                       'transducer': '',
                       'prefilter': ''}

            channel_info.append(ch_dict)
            data_list.append(channels[i])

        f.setTechnician('mednickdb')
        f.setSignalHeaders(channel_info)
        f.setStartdatetime(date)
        f.writeSamples(data_list)
    except Exception as e:
        raise IOError('EDF could not be written') from e

    finally:
        f.close()
    return True