"""
Default options and values for parsing.
Differs from pysleep defaults in that these settings are to do less with general sleep, and more with sac lab specifics
"""
# %% sleep arch
nighttime_split_method = None  # can be None (av across whole night), "quartiles" or "cycles" - TODO cycles

# %% sleep features
channels_to_extract_spindles_for = None  # None=all eeg, otherwise a list of chan names e.g. ['Cz', 'Pz']
channels_to_extract_sos_for = None  # None=all eeg, otherwise a list of chan names e.g. ['Cz', 'Pz']
channels_to_extract_rem_for = ['LOC', 'ROC']  # None=all eeg, otherwise a list of chan names e.g. ['Cz', 'Pz']

# %% std eeg
std_sfreq = 256
std_high_pass_hz = 0.3
std_low_pass_hz = 35



